﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Lab10
{
    public partial class fAdd : Form
    {
        public Team TheTeam;
        public fAdd(Team team)
        {
            TheTeam = team;
            InitializeComponent();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            bool parsed = true;
            TheTeam.name = tName.Text.Trim();
            TheTeam.country = tCountry.Text.Trim();
            TheTeam.stadium = tStadium.Text.Trim();
            int outParse;
            if (!int.TryParse(tPlayers.Text.Trim(), out outParse)) parsed = false;
            else TheTeam.players = outParse;
            if (!int.TryParse(tCoaches.Text.Trim(), out outParse)) parsed = false;
            else TheTeam.coaches = outParse;
            if (!int.TryParse(tWin.Text.Trim(), out outParse)) parsed = false;
            else TheTeam.win = outParse;
            if (!int.TryParse(tLose.Text.Trim(), out outParse)) parsed = false;
            else TheTeam.lose = outParse;
            if (parsed) DialogResult = DialogResult.OK;
            else MessageBox.Show("Перевірте правильність введення даних!");
        }
        
        private void fAdd_Load(object sender, EventArgs e)
        {
            if(TheTeam != null)
            {
                tName.Text = TheTeam.name;
                tCountry.Text = TheTeam.country;
                tStadium.Text = TheTeam.stadium;
                tPlayers.Text = TheTeam.players.ToString();
                tCoaches.Text = TheTeam.coaches.ToString();
                tWin.Text = TheTeam.win.ToString();
                tLose.Text = TheTeam.lose.ToString();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
