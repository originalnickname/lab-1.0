﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab10
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void fMain_Load(object sender, EventArgs e)
        {
            gridTeams.AutoGenerateColumns = false;

            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Name";
            column.Name = "Назва";
            gridTeams.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Country";
            column.Name = "Країна";
            gridTeams.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Stadium";
            column.Name = "Стадіон";
            gridTeams.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Players";
            column.Name = "Гравці";
            gridTeams.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Coaches";
            column.Name = "Тренери";
            gridTeams.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Win";
            column.Name = "Перемоги";
            gridTeams.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Lose";
            column.Name = "Поразки";
            gridTeams.Columns.Add(column);

            bindSrcTeam.Add(new Team("name","country","stad", 25,10,15,6));
            EventArgs args = new EventArgs();
            OnResize(args);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Team team = new Team();
            fAdd toAdd = new fAdd(team);
            if (toAdd.ShowDialog() == DialogResult.OK) bindSrcTeam.Add(team);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Team team = (Team)bindSrcTeam.List[bindSrcTeam.Position];
            fAdd toAdd = new fAdd(team);
            if (toAdd.ShowDialog() == DialogResult.OK) bindSrcTeam.List[bindSrcTeam.Position] = team;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Видалити поточний запис?", "Видалення запису", 
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            { bindSrcTeam.RemoveCurrent(); }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Очистити таблицю?\n\nВсі дані будуть втрачені", 
                "Очищення даних", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            { bindSrcTeam.Clear(); }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрити застосунок?", "Вихід з програми", 
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            { Application.Exit(); }
        }
    }
}

