﻿namespace Lab10
{
    partial class fAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tLose = new System.Windows.Forms.TextBox();
            this.tWin = new System.Windows.Forms.TextBox();
            this.tCoaches = new System.Windows.Forms.TextBox();
            this.tPlayers = new System.Windows.Forms.TextBox();
            this.tStadium = new System.Windows.Forms.TextBox();
            this.tCountry = new System.Windows.Forms.TextBox();
            this.tName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tLose);
            this.groupBox1.Controls.Add(this.tWin);
            this.groupBox1.Controls.Add(this.tCoaches);
            this.groupBox1.Controls.Add(this.tPlayers);
            this.groupBox1.Controls.Add(this.tStadium);
            this.groupBox1.Controls.Add(this.tCountry);
            this.groupBox1.Controls.Add(this.tName);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(10, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(258, 194);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Загальні дані";
            // 
            // tLose
            // 
            this.tLose.Location = new System.Drawing.Point(150, 165);
            this.tLose.Name = "tLose";
            this.tLose.Size = new System.Drawing.Size(100, 20);
            this.tLose.TabIndex = 13;
            // 
            // tWin
            // 
            this.tWin.Location = new System.Drawing.Point(150, 140);
            this.tWin.Name = "tWin";
            this.tWin.Size = new System.Drawing.Size(100, 20);
            this.tWin.TabIndex = 12;
            // 
            // tCoaches
            // 
            this.tCoaches.Location = new System.Drawing.Point(150, 115);
            this.tCoaches.Name = "tCoaches";
            this.tCoaches.Size = new System.Drawing.Size(100, 20);
            this.tCoaches.TabIndex = 11;
            // 
            // tPlayers
            // 
            this.tPlayers.Location = new System.Drawing.Point(150, 90);
            this.tPlayers.Name = "tPlayers";
            this.tPlayers.Size = new System.Drawing.Size(100, 20);
            this.tPlayers.TabIndex = 10;
            // 
            // tStadium
            // 
            this.tStadium.Location = new System.Drawing.Point(150, 65);
            this.tStadium.Name = "tStadium";
            this.tStadium.Size = new System.Drawing.Size(100, 20);
            this.tStadium.TabIndex = 9;
            // 
            // tCountry
            // 
            this.tCountry.Location = new System.Drawing.Point(150, 40);
            this.tCountry.Name = "tCountry";
            this.tCountry.Size = new System.Drawing.Size(100, 20);
            this.tCountry.TabIndex = 8;
            // 
            // tName
            // 
            this.tName.Location = new System.Drawing.Point(150, 15);
            this.tName.Name = "tName";
            this.tName.Size = new System.Drawing.Size(100, 20);
            this.tName.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 170);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Кількість поразок";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Кількість перемог";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Кількість тренерів";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Кількість гравців";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Домашній стадіон";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Країна команди";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Назва команди";
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(283, 17);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 14;
            this.btnNew.Text = "OK";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(283, 45);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Скасувати";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // fAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 218);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "fAdd";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Додати об\'єкт";
            this.Load += new System.EventHandler(this.fAdd_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tLose;
        private System.Windows.Forms.TextBox tWin;
        private System.Windows.Forms.TextBox tCoaches;
        private System.Windows.Forms.TextBox tPlayers;
        private System.Windows.Forms.TextBox tStadium;
        private System.Windows.Forms.TextBox tCountry;
        private System.Windows.Forms.TextBox tName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnCancel;
    }
}