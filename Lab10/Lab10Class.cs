﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab10
{
    public class Team
    {
        public string name { get; set; }
        public string country { get; set; }
        public string stadium { get; set; }
        public int players { get; set; }
        public int coaches { get; set; }
        public int win { get; set; }
        public int lose { get; set; }
        public Team() {
            name = "N/S";
            country = "N/S";
            stadium = "N/S";
        }
        public Team (string name_, string country_, string stadium_, int players_, int coaches_, int win_, int lose_)
        {
            name = name_;
            country = country_;
            stadium = stadium_;
            players = players_;
            coaches = coaches_;
            win = win_;
            lose = lose_;
        }
    }
}
