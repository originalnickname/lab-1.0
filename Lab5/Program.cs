﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5
{
    public class Team
    {
        protected string name;
            public void Name(string name_)
        {
            this.name = name_;
        }
            public string Name()
        {
            return this.name;
        }

        protected string country;
            public void Country(string country_)
        {
            this.country = country_;
        }
            public string Country()
        {
            return this.country;
        }

        protected string stadium;
            public void Stadium(string stadium_)
        {
            this.stadium = stadium_;
        }
            public string Stadium()
        {
            return this.stadium;
        }

        protected int players;
            public void Players(int players_)
        {
            this.players = players_;
        }
            public int Players()
        {
            return this.players;
        }

        protected int coaches;
            public void Coaches(int coaches_)
        {
            this.coaches = coaches_;
        }
            public int Coaches()
        {
            return this.coaches;
        }

        protected int yearwins;
            public void YearWins(int yearwins_)
        {
            this.yearwins = yearwins_;
        }
            public int YearWins()
        {
            return this.yearwins;
        }

        protected int yearlose;
            public void YearLose(int yearlose_)
        {
            this.yearlose = yearlose_;
        }
            public int YearLose()
        {
            return this.yearlose;
        }

        public Team()
        {
            this.name = "N/S";
            this.country = "N/S";
            this.stadium = "N/S";
            this.players = 0;
            this.coaches = 0;
            this.yearwins = 0;
            this.yearlose = 0;
        }

        public void Print()
        {
            Console.WriteLine("Team {0}. Country - {1}, stadium - {2}.\nHas {3} players and {4} coaches.\nWon {5} games and lost {6} this year.", this.name, this.country, this.stadium, this.players, this.coaches, this.yearwins, this.yearlose);
        }

        public void Win() => ++this.yearwins;

        public void Lose() => ++this.yearlose;
        
        public double WinrD()
        {
            return (double)this.yearwins / this.yearlose;
        }

        public void WinRate ()
        {
            Console.WriteLine("Won : {0}; Lost : {1}; Winrate = {2:0.00}", this.yearwins, this.yearlose, this.WinrD());
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Team team = new Team();
            Console.WriteLine("Team created");
            while (true)
            {
                Console.Write("\n\n");
                PrintMainMenu();
                ConsoleKeyInfo mainm = Console.ReadKey();
                Console.Write("\n\n");
                if (mainm.Key == ConsoleKey.D1 || mainm.Key == ConsoleKey.NumPad1) team.Print();
                else if (mainm.Key == ConsoleKey.D2 || mainm.Key == ConsoleKey.NumPad2) team.WinRate();
                else if (mainm.Key == ConsoleKey.D3 || mainm.Key == ConsoleKey.NumPad3) team.Win();
                else if (mainm.Key == ConsoleKey.D4 || mainm.Key == ConsoleKey.NumPad4) team.Lose();
                else if (mainm.Key == ConsoleKey.D5 || mainm.Key == ConsoleKey.NumPad5)
                {
                    while (true)
                    {
                        Console.WriteLine("\n\n");
                        Console.WriteLine("\t[1] - Edit Name;");
                        Console.WriteLine("\t[2] - Edit Country;");
                        Console.WriteLine("\t[3] - Edit Stadium;");
                        Console.WriteLine("\t[4] - Edit Players number;");
                        Console.WriteLine("\t[5] - Edit Coaches number;");
                        Console.WriteLine("\t[6] - Edit Year Wins;");
                        Console.WriteLine("\t[7] - Edit Year Loses;");
                        Console.WriteLine("\t[ESC] - Back to Main;");
                        Console.Write("\tYour choice : ");

                        ConsoleKeyInfo editing = Console.ReadKey();

                        if (editing.Key == ConsoleKey.D1 || editing.Key == ConsoleKey.NumPad1)
                        {
                            Console.Write("\n\n\tCurrent Name value : ");
                            Console.WriteLine(team.Name());
                            Console.Write("\n\tEnter the value to replace: ");
                            string toreplace = Console.ReadLine();
                            team.Name(toreplace);
                            Console.Write("\n\tCurrent value : ");
                            Console.WriteLine(team.Name());
                        }
                        else if (editing.Key == ConsoleKey.D2 || editing.Key == ConsoleKey.NumPad2)
                        {
                            Console.Write("\n\n\tCurrent Country value : ");
                            Console.WriteLine(team.Country());
                            Console.Write("\n\tEnter the value to replace: ");
                            string toreplace = Console.ReadLine();
                            team.Country(toreplace);
                            Console.Write("\n\tCurrent value : ");
                            Console.WriteLine(team.Country());
                        }
                        else if (editing.Key == ConsoleKey.D3 || editing.Key == ConsoleKey.NumPad3)
                        {
                            Console.Write("\n\n\tCurrent Stadium value : ");
                            Console.WriteLine(team.Stadium());
                            Console.Write("\n\tEnter the value to replace: ");
                            string toreplace = Console.ReadLine();
                            team.Stadium(toreplace);
                            Console.Write("\n\tCurrent value : ");
                            Console.WriteLine(team.Stadium());
                        }
                        else if (editing.Key == ConsoleKey.D4 || editing.Key == ConsoleKey.NumPad4)
                        {
                            Console.Write("\n\n\tCurrent Players value : ");
                            Console.WriteLine(team.Players());
                            Console.Write("\n\tEnter the value to replace: ");
                            int toreplace = int.Parse(Console.ReadLine());
                            team.Players(toreplace);
                            Console.Write("\n\tCurrent value : ");
                            Console.WriteLine(team.Players());
                        }
                        else if (editing.Key == ConsoleKey.D5 || editing.Key == ConsoleKey.NumPad5)
                        {
                            Console.Write("\n\n\tCurrent Coaches value : ");
                            Console.WriteLine(team.Coaches());
                            Console.Write("\n\tEnter the value to replace: ");
                            int toreplace = int.Parse(Console.ReadLine());
                            team.Coaches(toreplace);
                            Console.Write("\n\tCurrent value : ");
                            Console.WriteLine(team.Coaches());
                        }
                        else if (editing.Key == ConsoleKey.D6 || editing.Key == ConsoleKey.NumPad6)
                        {
                            Console.Write("\n\n\tCurrent Year Wins value : ");
                            Console.WriteLine(team.YearWins());
                            Console.Write("\n\tEnter the value to replace: ");
                            int toreplace = int.Parse(Console.ReadLine());
                            team.YearWins(toreplace);
                            Console.Write("\n\tCurrent value : ");
                            Console.WriteLine(team.YearWins());
                        }
                        else if (editing.Key == ConsoleKey.D7 || editing.Key == ConsoleKey.NumPad7)
                        {
                            Console.Write("\n\n\tCurrent Year Lose value : ");
                            Console.WriteLine(team.YearLose());
                            Console.Write("\n\tEnter the value to replace: ");
                            int toreplace = int.Parse(Console.ReadLine());
                            team.YearLose(toreplace);
                            Console.Write("\n\tCurrent value : ");
                            Console.WriteLine(team.YearLose());
                        }
                        else if (editing.Key == ConsoleKey.Escape) break;
                        else Console.WriteLine("\n\tWrong input!");
                    }
                }  //EDITING
                else if (mainm.Key == ConsoleKey.Escape) break;
                else Console.WriteLine("\n\nWrong input!");
            }
        }

        static void PrintMainMenu ()
        {
            Console.WriteLine("\n[1] - Team info;");
            Console.WriteLine("[2] - Team stats;");
            Console.WriteLine("[3] - Win;");
            Console.WriteLine("[4] - Lose;");
            Console.WriteLine("[5] - Editing;");
            Console.WriteLine("[ESC] - Exit;");
            Console.Write("Your choice : ");
        }
    }
}
