﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class Program
    {
        const double StartX = 0;
        const double dX = 0.05;
        static double Function(double x)
        {
            double cosx = Math.Cos(2.76 * x);
            return Math.Sqrt(cosx*cosx*cosx + x / 2) / (Math.Pow(2.76 * x, 13) + 3 / Math.Cos(x / 2));
        }

        static void Main(string[] args)
        {
            double[] arr = new double[10];
            double x = StartX;

            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
            {
                arr[i] = Function(x);
                x += dX;
            }

            Array.Sort(arr);
            Array.Reverse(arr);
            Console.WriteLine("Вiдсортованi за спаданням значення масиву: ");

            foreach (int i in arr)
            {
                Console.WriteLine("arr[{0:00}] = {1:0.00000}", i, arr[i]);
            }
            double aMin = arr[arr.GetUpperBound(0)];
            double aMax = arr[arr.GetLowerBound(0)];
            double aAvg = 0;

            foreach (int i in arr)
            {
                aAvg += arr[i];
            }
            aAvg = aAvg / arr.GetLength(0);
            Console.WriteLine("Мiнiмальне значення масиву: {0:0.00000}", aMin);
            Console.WriteLine("Максимальне значення масиву: {0:0.00000}", aMax);
            Console.WriteLine("Середнє значення масиву: {0:0.00000}", aAvg);
            int avgCnt = 0;
            double lbound = 0.9 * aAvg, ubound = 1.1 * aAvg;
            foreach(double cnt in arr)
            {
                if (cnt >= lbound && cnt <= ubound) ++avgCnt;
            }

            Console.WriteLine("Кiлькiсть елементiв, що знаходяться в 10% вiд середнього значення: {0}", avgCnt);
            Console.ReadKey();
        }
    }
}
