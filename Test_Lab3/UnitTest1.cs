﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab3;

namespace Test_Lab3
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Func_check()
        {
            double x = 1;
            double exp = 0.0523850;
            double real = Math.Round(Lab3.Program.FuncInt(x), 6);
            Assert.AreEqual(exp, real);
        }
        [TestMethod]
        public void Trap_check() {
            double start = 1, end = 10, div = (end - start) / 100;
            double expected = 0.141141;
            double actual = Math.Round(Lab3.Program.Trap(start, end, div, Lab3.Program.FuncInt), 6);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Left_check() {
            double start = 1, end = 10, div = (end - start) / 100;
            double expected = 0.143091;
            double actual = Math.Round(Lab3.Program.Left(start, end, div, Lab3.Program.FuncInt), 6);
            System.Diagnostics.Debug.WriteLine(actual);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Right_check() {
            double start = 1, end = 10, div = (end - start) / 100;
            double expected = 0.138377;
            double actual = Math.Round(Lab3.Program.Right(start, end, div, Lab3.Program.FuncInt), 6);
            System.Diagnostics.Debug.WriteLine(actual);
            Assert.AreEqual(expected, actual);
        }
    }
}
