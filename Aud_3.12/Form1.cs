﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aud_3._12
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<string> normText = new List<string>();

        private void btnLoad_Click(object sender, EventArgs e)
        {
            string[] text = System.IO.File.ReadAllLines("..\\..\\toRead.txt");
            foreach(string a in text)
            {
                TextBox.Text += a;
            }
            foreach(string a in text)
            {
                normText.Add(a);
            }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            string edit = Interaction.InputBox("Enter the value to filter", "Input Required");
            for(int i = 0; i < normText.Count; ++i)
            {
                if (normText[i].Contains(edit)) normText.Remove(normText[i--]);
                
            }
            TextBox.Text = "";
            foreach(string a in normText)
            {
                TextBox.Text += a;
            }
        }
    }
}
