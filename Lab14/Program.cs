﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab14
{
    class Program
    {
        static List<Team> teams;
        static void Main(string[] args)
        {
            Initialize();
            Console.WriteLine("Done. Loading menu...");
            ConsoleKeyInfo mainMenu;
            while (true)
            {
                PrintMainMenu();
                mainMenu = Console.ReadKey();
                if (mainMenu.Key == ConsoleKey.Escape) break;
                else if (mainMenu.Key == ConsoleKey.D1)
                {
                    Console.Clear();
                    Console.WriteLine("Your choice: Print.");
                    Print();
                }
                else if (mainMenu.Key == ConsoleKey.D2)
                {
                    Console.Clear();
                    Console.WriteLine("Your choice: Sort.");
                    teams.Sort();
                    Console.WriteLine("\nSorted.", teams.Count);
                    Print();
                }
                else if (mainMenu.Key == ConsoleKey.D3)
                {
                    Console.Clear();
                    Console.WriteLine("Your choice: Add.");
                    AddTeam();
                }
                else if (mainMenu.Key == ConsoleKey.D4)
                {
                    Console.Clear();
                    Console.WriteLine("Your choice: Edit.");
                    int [] edit = EditMenu();
                    if(edit[1] != 9) Edit(edit[0], edit[1]);
                }
                else Console.WriteLine("\n\nWrong Input!\n");
                
            }
        }
        static void Initialize()
        {
            teams = new List<Team>();
            FileStream fs = new FileStream("..\\..\\123.towns", FileMode.Open);
            BinaryReader br = new BinaryReader(fs);
            try
            {
                Team team;
                Console.WriteLine("Reading data...\n");
                while (br.BaseStream.Position < br.BaseStream.Length)
                {
                    team = new Team();
                    for (int i = 1; i <= 7; ++i)
                    {
                        if (i == 1) { team.name = br.ReadString(); }
                        else if (i == 2) { team.country = br.ReadString(); }
                        else if (i == 3) { team.stadium = br.ReadString(); }
                        else if (i == 4) { team.players = br.ReadInt32(); }
                        else if (i == 5) { team.coaches = br.ReadInt32(); }
                        else if (i == 6) { team.win = br.ReadInt32(); }
                        else { team.lose = br.ReadInt32(); }
                    }
                    teams.Add(team);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: {0}", ex.Message);
            }
            finally
            {
                br.Close();
            }
        }
        static void Print()
        {
            Console.WriteLine("\nElements: {0}:", teams.Count);
            int i = 1;
            foreach (Team t in teams) {
                Console.Write("[{0}] - ", i++);
                Console.WriteLine(t.ToString());
            }
            Console.WriteLine();
        }
        static void PrintMainMenu()
        {
            Console.Write("Press any key to continue");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("********MAIN MENU********");
            Console.WriteLine("  1  - Print all teams");
            Console.WriteLine("  2  - Sort teams");
            Console.WriteLine("  3  - Add team");
            Console.WriteLine("  4  - Edit team");
            Console.WriteLine(" ESC - Exit");
            Console.Write("\nYour choice: ");
        }
        static void AddTeam ()
        {
            Console.WriteLine();
            string name = Input("name");
            string country = Input("country");
            string stadium = Input("stadium");
            bool nice = false;
            int players;
            nice = int.TryParse(Input("players"), out players);
            if (!nice) { Console.WriteLine("Wrong input!"); return; }
            int coaches;
            nice = int.TryParse(Input("coaches"), out coaches);
            if (!nice) { Console.WriteLine("Wrong input!"); return; }
            int wins;
            nice = int.TryParse(Input("wins"), out wins);
            if (!nice) { Console.WriteLine("Wrong input!"); return; }
            int losses;
            nice = int.TryParse(Input("losses"), out losses);
            if (!nice) { Console.WriteLine("Wrong input!"); return; }
            Team team = new Team(name, country, stadium, players, coaches, wins, losses);
            teams.Add(team);
            Console.WriteLine("Added.\n");
        }

        static int[] EditMenu()
        {
            int[] arr = new int[2];
            Console.WriteLine("\nEnter the index of team (1 - {0}) to replace.", teams.Count);
            Console.WriteLine("Enter 0 to view all the teams.");
            Console.Write("\nYour choice: ");
            while (true)
            {
                int index = -1;
                if (int.TryParse(Console.ReadLine(), out index) && index > 0 && index <= teams.Count)
                {
                    arr[0] = --index;
                    break;
                }
                else if (index == 0) {
                    Console.Clear();
                    Console.WriteLine("Your choice: Edit.");
                    Print();
                    Console.Write("\nEnter index (1 - {0}) to edit: ",teams.Count);
                }
                else Console.Write("Wrong input! Expected 0 - {0}.\nTry again:", teams.Count);
            }
            Console.Clear();
            Console.WriteLine("Selected index - {0}:\n{1}\n",arr[0] + 1, teams.ElementAt(arr[0]).ToString());
            Console.WriteLine(" 1 - Remove team");
            Console.WriteLine(" 2 - Edit name");
            Console.WriteLine(" 3 - Edit country");
            Console.WriteLine(" 4 - Edit stadium");
            Console.WriteLine(" 5 - Edit players");
            Console.WriteLine(" 6 - Edit coaches");
            Console.WriteLine(" 7 - Edit wins");
            Console.WriteLine(" 8 - Edit losses");
            Console.WriteLine(" 9 - No editing");
            Console.Write("\nYour chioce: ");
            int res;
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out res) && res > 0 && res < 10)
                {
                    arr[1] = res;
                    break;
                }
                else Console.WriteLine("Wrong input!\nTry again: ");
            }
            return arr;
        }

        static void Edit(int index, int toDo)
        {
            if (toDo == 1) { teams.RemoveAt(index); }
            else if (toDo == 2)
            {
                Console.Clear();
                Console.Write("Your choice: Edit({0}, name).\n\nCurrent value: {1}.\n",index, teams.ElementAt(index).name);
                teams.ElementAt(index).name = Input("name");
            }
            else if (toDo == 3)
            {
                Console.Clear();
                Console.Write("Your choice: Edit({0}, country).\n\nCurrent value: {1}.\n", index, teams.ElementAt(index).country);
                teams.ElementAt(index).country = Input("country");
            }
            else if (toDo == 4) {
                Console.Clear();
                Console.Write("Your choice: Edit({0}, stadium).\n\nCurrent value: {1}.\n", index, teams.ElementAt(index).stadium);
                teams.ElementAt(index).stadium = Input("stadium"); }
            else if (toDo == 5)
            {
                Console.Clear();
                Console.Write("Your choice: Edit({0}, players).\n\nCurrent value: {1}.\n", index, teams.ElementAt(index).players);
                int players;
                while (true)
                {
                    if (!int.TryParse(Input("players"), out players))
                    {
                        Console.WriteLine("Wrong input!\nTry again: ");
                    }
                    else break;
                }
                teams.ElementAt(index).players = players;
            }
            else if (toDo == 6)
            {
                Console.Clear();
                Console.Write("Your choice: Edit({0}, coaches).\n\nCurrent value: {1}.\n", index, teams.ElementAt(index).coaches);
                int coaches;
                while (true)
                {
                    if (!int.TryParse(Input("coaches"), out coaches))
                    {
                        Console.WriteLine("Wrong input!\nTry again: ");
                    }
                    else break;
                }
                teams.ElementAt(index).coaches = coaches;
            }
            else if (toDo == 7)
            {
                Console.Clear();
                Console.Write("Your choice: Edit({0}, wins).\n\nCurrent value: {1}.\n", index, teams.ElementAt(index).win);
                int wins;
                while (true)
                {
                    if (!int.TryParse(Input("wins"), out wins))
                    {
                        Console.WriteLine("Wrong input!\nTry again: ");
                    }
                    else break;
                }
                teams.ElementAt(index).win = wins;
            }
            else if (toDo == 8)
            {
                Console.Clear();
                Console.Write("Your choice: Edit({0}, losses).\n\nCurrent value: {1}.\n", index, teams.ElementAt(index).lose);
                int losses;
                while (true)
                {
                    if (!int.TryParse(Input("losses"), out losses))
                    {
                        Console.WriteLine("Wrong input!\nTry again: ");
                    }
                    else break;
                }
                teams.ElementAt(index).lose = losses;
            }

        }
        static string Input(string type)
        {
            Console.Write("Enter {0}: ", type);
            return Console.ReadLine();
        }
    }
}
