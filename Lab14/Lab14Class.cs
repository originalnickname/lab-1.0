﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    public class Team : IComparable
    {
        public string name { get; set; }
        public string country { get; set; }
        public string stadium { get; set; }
        public int players { get; set; }
        public int coaches { get; set; }
        public int win { get; set; }
        public int lose { get; set; }
        public Team() {
            name = "N/S";
            country = "N/S";
            stadium = "N/S";
        }
        public Team (string name_, string country_, string stadium_, int players_, int coaches_, int win_, int lose_)
        {
            name = name_;
            country = country_;
            stadium = stadium_;
            players = players_;
            coaches = coaches_;
            win = win_;
            lose = lose_;
        }
        public override string ToString() {
            return name + "," + country + "," + stadium + "," + 
                   players.ToString() + "," + coaches.ToString() + "," +
                   win.ToString() + "," + lose.ToString();
        }
        public int CompareTo (object obj)
        {
            Team t = obj as Team;
            return string.Compare(name, t.name);
        }

    }
}
