﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab08Class
{
    public class Team
    {
        protected string name;
        public void Name(string name_)
        {
            this.name = name_;
        }
        public string Name()
        {
            return this.name;
        }

        protected string country;
        public void Country(string country_)
        {
            this.country = country_;
        }
        public string Country()
        {
            return this.country;
        }

        protected string stadium;
        public void Stadium(string stadium_)
        {
            this.stadium = stadium_;
        }
        public string Stadium()
        {
            return this.stadium;
        }

        protected int players;
        public void Players(int players_)
        {
            this.players = players_;
        }
        public int Players()
        {
            return this.players;
        }

        protected int coaches;
        public void Coaches(int coaches_)
        {
            this.coaches = coaches_;
        }
        public int Coaches()
        {
            return this.coaches;
        }

        protected int yearwins;
        public void YearWins(int yearwins_)
        {
            this.yearwins = yearwins_;
        }
        public int YearWins()
        {
            return this.yearwins;
        }

        protected int yearlose;
        public void YearLose(int yearlose_)
        {
            this.yearlose = yearlose_;
        }
        public int YearLose()
        {
            return this.yearlose;
        }

        public Team()
        {
            this.name = "N/S";
            this.country = "N/S";
            this.stadium = "N/S";
            this.players = 0;
            this.coaches = 0;
            this.yearwins = 0;
            this.yearlose = 0;
        }
        public Team(string name_, string country_, string stadium_, int players_, int coaches_, int wins_, int lose_)
        {
            this.name = name_;
            this.country = country_;
            this.stadium = stadium_;
            this.players = players_;
            this.coaches = coaches_;
            this.yearwins = wins_;
            this.yearlose = lose_;
        }

        public string Print()
        {

            return "Team {0}. " + this.name
                    + "Country - " + this.country
                    + ", stadium - " + this.stadium + "."
                    + "\nHas " + this.players.ToString() + " players"
                    + " and " + this.coaches.ToString() + " coaches"
                    + "."
                    + "\nWon " + this.yearwins.ToString() + " games"
                    + " and lost " + this.yearlose.ToString() + " this year.\n";
        }
    }
}
