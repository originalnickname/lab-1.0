﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.IO;

namespace Lab13
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void fMain_Load(object sender, EventArgs e)
        {
            gridTeams.AutoGenerateColumns = false;

            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Name";
            column.Name = "Назва";
            gridTeams.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Country";
            column.Name = "Країна";
            gridTeams.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Stadium";
            column.Name = "Стадіон";
            gridTeams.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Players";
            column.Name = "Гравці";
            gridTeams.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Coaches";
            column.Name = "Тренери";
            gridTeams.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Win";
            column.Name = "Перемоги";
            gridTeams.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Lose";
            column.Name = "Поразки";
            gridTeams.Columns.Add(column);

            bindSrcTeam.Add(new Team("name", "country", "stad", 25, 10, 15, 6));
            EventArgs args = new EventArgs();
            OnResize(args);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Team team = new Team();
            fAdd toAdd = new fAdd(team);
            if (toAdd.ShowDialog() == DialogResult.OK) bindSrcTeam.Add(team);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Team team = (Team)bindSrcTeam.List[bindSrcTeam.Position];
            fAdd toAdd = new fAdd(team);
            if (toAdd.ShowDialog() == DialogResult.OK) bindSrcTeam.List[bindSrcTeam.Position] = team;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Видалити поточний запис?", "Видалення запису",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            { bindSrcTeam.RemoveCurrent(); }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Очистити таблицю?\n\nВсі дані будуть втрачені",
                "Очищення даних", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            { bindSrcTeam.Clear(); }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрити застосунок?", "Вихід з програми",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            { Application.Exit(); }
        }

        private void btnSaveAsText_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Text file (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.Title = "Зберегти дані у текстовому форматі";
            saveFileDialog.InitialDirectory = Application.StartupPath;
            StreamWriter sw;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                sw = new StreamWriter(saveFileDialog.FileName, false, Encoding.UTF8);
                try
                {
                    foreach (Team team in bindSrcTeam.List)
                    {
                        sw.Write(
                            team.name
                            + "\t" +
                            team.country
                            + "\t" +
                            team.stadium
                            + "\t" +
                            team.players
                            + "\t" +
                            team.coaches
                            + "\t" +
                            team.win
                            + "\t" +
                            team.lose
                            + "\t\n");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally { sw.Close(); }
            }
        }

        private void btnSaveAsBinary_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Файли даних (*.towns)|*.towns|All files (*.*)|*.*";
            saveFileDialog.Title = "Зберегти дані у бінарному форматі";
            saveFileDialog.InitialDirectory = Application.StartupPath;
            BinaryWriter bw;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                bw = new BinaryWriter(saveFileDialog.OpenFile());
                try
                {
                    foreach (Team team in bindSrcTeam.List)
                    {
                        bw.Write(team.name);
                        bw.Write(team.country);
                        bw.Write(team.stadium);
                        bw.Write(team.players);
                        bw.Write(team.coaches);
                        bw.Write(team.win);
                        bw.Write(team.lose);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally { bw.Close(); }
            }
        }

        private void btnOpenFromText_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Text (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog.Title = "Прочитати дані у текстовому форматі";
            openFileDialog.InitialDirectory = Application.StartupPath;
            StreamReader sr;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                bindSrcTeam.Clear();
                sr = new StreamReader(openFileDialog.FileName, Encoding.UTF8);
                string s;
                try {
                    while ((s = sr.ReadLine()) != null)
                    {
                        string[] split = s.Split('\t');
                        Team town = new Team(split[0], split[1], split[2], int.Parse(split[3]), int.Parse(split[4]), int.Parse(split[5]), int.Parse(split[6]));
                        bindSrcTeam.Add(town);
                    }
                }
                catch (Exception ex) {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally { sr.Close(); } }
        }

        private void btnOpenFromBinary_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Binary (*.towns)|*.towns|All files (*.*)|*.*";
            openFileDialog.Title = "Прочитати дані у бінарному форматі";
            openFileDialog.InitialDirectory = Application.StartupPath;
            BinaryReader br;
            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                bindSrcTeam.Clear();
                br = new BinaryReader(openFileDialog.OpenFile());
                try
                {
                    Team team;
                    while (br.BaseStream.Position < br.BaseStream.Length)
                    {
                        team = new Team();
                        for (int i = 1; i <= 7; ++i)
                        {
                            if (i == 1) { team.name = br.ReadString(); }
                            else if (i == 2) { team.country = br.ReadString(); }
                            else if (i == 3) { team.stadium = br.ReadString(); }
                            else if (i == 4) { team.players = br.ReadInt32(); }
                            else if (i == 5) { team.coaches = br.ReadInt32(); }
                            else if (i == 6) { team.win = br.ReadInt32(); }
                            else { team.lose = br.ReadInt32(); }
                        }
                        bindSrcTeam.Add(team);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally { br.Close(); }
            }
        }
    }
}

