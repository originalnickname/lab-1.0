﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab08Class;

namespace Lab08
{
    public partial class fAdd : Form
    {
        public Team TheTeam;
        public fAdd(Team team)
        {
            TheTeam = team;
            InitializeComponent();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            bool parsed = true;
            TheTeam.Name(tName.Text.Trim());
            TheTeam.Country(tCountry.Text.Trim());
            TheTeam.Stadium(tStadium.Text.Trim());
            int outParse;
            if (!int.TryParse(tPlayers.Text.Trim(), out outParse)) parsed = false;
            else TheTeam.Players(outParse);
            if (!int.TryParse(tCoaches.Text.Trim(), out outParse)) parsed = false;
            else TheTeam.Coaches(outParse);
            if (!int.TryParse(tWin.Text.Trim(), out outParse)) parsed = false;
            else TheTeam.YearWins(outParse);
            if (!int.TryParse(tLose.Text.Trim(), out outParse)) parsed = false;
            else TheTeam.YearLose(outParse);
            if (parsed) DialogResult = DialogResult.OK;
            else MessageBox.Show("Перевірте правильність введення даних!");
        }
        
        private void fAdd_Load(object sender, EventArgs e)
        {
            if(TheTeam != null)
            {
                tName.Text = TheTeam.Name();
                tCountry.Text = TheTeam.Country();
                tStadium.Text = TheTeam.Stadium();
                tPlayers.Text = TheTeam.Players().ToString();
                tCoaches.Text = TheTeam.Coaches().ToString();
                tWin.Text = TheTeam.YearWins().ToString();
                tLose.Text = TheTeam.YearLose().ToString();
            }
        }

    }
}
