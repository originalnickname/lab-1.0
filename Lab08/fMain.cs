﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab08Class;

namespace Lab08
{
    public partial class fMain : Form
    {
        
        public fMain()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Припинити роботу?", "Припинення роботи", 
                MessageBoxButtons.OKCancel,MessageBoxIcon.Question) == DialogResult.OK) Application.Exit(); 
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Team team = new Team();
            fAdd addTeam = new fAdd(team);
            if (addTeam.ShowDialog() == DialogResult.OK) tbFull.Text += team.Print();
        }
    }
}
