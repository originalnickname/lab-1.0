﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab7;

namespace Lab7
{
    public partial class fMain : Form
    {

        public static double Function(double x1, double x2)
        {
            return Math.Cos(x2 / x1) / (4 + x2);
        }
        public static double Variant(double x1, double x2)
        {
            if (x1 > x2) return x1;
            else return x2;
        }
        public fMain()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ext_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void clr_Click(object sender, EventArgs e)
        {
            inp1.Text = string.Empty;
            inp2.Text = string.Empty;
            outp.Text = string.Empty;
            outvar.Text = string.Empty;
        }

        private void count_Click(object sender, EventArgs e)
        {
            double x1, x2;
            if (string.IsNullOrEmpty(inp1.Text) || (String.IsNullOrEmpty(inp2.Text))) {
                outp.Text = "Не введено даних!";
                outvar.Text = "Не введено даних!"; return;
            } else if (!double.TryParse(inp1.Text, out x1) || !double.TryParse(inp2.Text, out x2)) {
                outp.Text = "Неправильні дані!";
                outvar.Text = "Неправильні дані!"; return;
            }
            else {
                double y = Function(x1, x2);
                outp.Text = y.ToString("0.#########e+00");
            }
            outvar.Text = Variant(x1, x2).ToString("0.##########e+00");
        }
    }
}
