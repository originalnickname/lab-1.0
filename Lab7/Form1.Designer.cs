﻿namespace Lab7
{
    partial class fMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.inp1 = new System.Windows.Forms.TextBox();
            this.inp2 = new System.Windows.Forms.TextBox();
            this.outp = new System.Windows.Forms.TextBox();
            this.count = new System.Windows.Forms.Button();
            this.clr = new System.Windows.Forms.Button();
            this.ext = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.outvar = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Змінна Х1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Змінна Х2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Результат";
            // 
            // inp1
            // 
            this.inp1.Location = new System.Drawing.Point(110, 17);
            this.inp1.Name = "inp1";
            this.inp1.Size = new System.Drawing.Size(145, 20);
            this.inp1.TabIndex = 3;
            // 
            // inp2
            // 
            this.inp2.Location = new System.Drawing.Point(110, 47);
            this.inp2.Name = "inp2";
            this.inp2.Size = new System.Drawing.Size(145, 20);
            this.inp2.TabIndex = 4;
            // 
            // outp
            // 
            this.outp.Location = new System.Drawing.Point(110, 77);
            this.outp.Name = "outp";
            this.outp.ReadOnly = true;
            this.outp.Size = new System.Drawing.Size(145, 20);
            this.outp.TabIndex = 5;
            // 
            // count
            // 
            this.count.Location = new System.Drawing.Point(19, 141);
            this.count.Name = "count";
            this.count.Size = new System.Drawing.Size(75, 23);
            this.count.TabIndex = 6;
            this.count.Text = "Обчислити";
            this.count.UseVisualStyleBackColor = true;
            this.count.Click += new System.EventHandler(this.count_Click);
            // 
            // clr
            // 
            this.clr.Location = new System.Drawing.Point(100, 141);
            this.clr.Name = "clr";
            this.clr.Size = new System.Drawing.Size(75, 23);
            this.clr.TabIndex = 7;
            this.clr.Text = "Очистити";
            this.clr.UseVisualStyleBackColor = true;
            this.clr.Click += new System.EventHandler(this.clr_Click);
            // 
            // ext
            // 
            this.ext.Location = new System.Drawing.Point(181, 141);
            this.ext.Name = "ext";
            this.ext.Size = new System.Drawing.Size(75, 23);
            this.ext.TabIndex = 8;
            this.ext.Text = "Вихід";
            this.ext.UseVisualStyleBackColor = true;
            this.ext.Click += new System.EventHandler(this.ext_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Більше із знач.";
            // 
            // outvar
            // 
            this.outvar.Location = new System.Drawing.Point(111, 109);
            this.outvar.Name = "outvar";
            this.outvar.ReadOnly = true;
            this.outvar.Size = new System.Drawing.Size(145, 20);
            this.outvar.TabIndex = 10;
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(276, 176);
            this.Controls.Add(this.outvar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ext);
            this.Controls.Add(this.clr);
            this.Controls.Add(this.count);
            this.Controls.Add(this.outp);
            this.Controls.Add(this.inp2);
            this.Controls.Add(this.inp1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "fMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабораторна робота 7";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox inp1;
        private System.Windows.Forms.TextBox inp2;
        private System.Windows.Forms.TextBox outp;
        private System.Windows.Forms.Button count;
        private System.Windows.Forms.Button clr;
        private System.Windows.Forms.Button ext;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox outvar;
    }
}

