﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab09
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        public double Funct (double x1, double x2)
        {
            return Math.Cos(x2 / x1) / (4 + x2);
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            double x1min = double.Parse(tbx1min.Text, System.Globalization.CultureInfo.InvariantCulture);
            double x1max = double.Parse(tbx1max.Text, System.Globalization.CultureInfo.InvariantCulture);
            double x2min = double.Parse(tbx2min.Text, System.Globalization.CultureInfo.InvariantCulture);
            double x2max = double.Parse(tbx2max.Text, System.Globalization.CultureInfo.InvariantCulture);
            double dx1 = double.Parse(tbdx1.Text, System.Globalization.CultureInfo.InvariantCulture);
            double dx2 = double.Parse(tbdx2.Text, System.Globalization.CultureInfo.InvariantCulture);
            double midres, res = 0;
            grid.ColumnCount = (int)Math.Truncate((x2max - x2min) / dx2) + 1;
            grid.RowCount = (int)Math.Truncate((x1max - x1min) / dx1) + 1;

            for (int i = 0; i < grid.RowCount; i++)
            {
                grid.Rows[i].HeaderCell.Value = (x1min + i * dx1).ToString("0.000");
                grid.RowHeadersWidth = 80;
            }
            for (int i = 0; i < grid.ColumnCount; i++) {
                grid.Columns[i].HeaderCell.Value = (x2min + i * dx2).ToString("0.000");
                grid.Columns[i].Width = 60;
            }

            int rw= 0, cl;
            double x1, x2, y;
            for(x1 = x1min; x1 <= x1max; x1 += dx1)
            {
                cl = 0;
                for(x2 = x2min; x2 <= x2max; x2 += dx2)
                {
                    y = Funct(x1, x2);
                    if((midres = Math.Cos(y)) > 0) res += midres;
                    grid.Rows[rw].Cells[cl].Value = y.ToString("0.000");
                    ++cl;
                }
                ++rw;
            }
            tbVariant.Text = res.ToString("0.0000e+00");
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tbx1min.Text = "";
            tbx1max.Text = "";
            tbx2min.Text = "";
            tbx2max.Text = "";
            tbdx1.Text = "";
            tbdx2.Text = "";
            grid.Rows.Clear();
            for (int c = 0; c < grid.ColumnCount; ++c) grid.Columns[c].HeaderCell.Value = ""; 
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
