﻿namespace Lab09
{
    partial class fMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.grid = new System.Windows.Forms.DataGridView();
            this.tbx1min = new System.Windows.Forms.TextBox();
            this.lbx1max = new System.Windows.Forms.Label();
            this.lbx2max = new System.Windows.Forms.Label();
            this.lbx1min = new System.Windows.Forms.Label();
            this.lbx2min = new System.Windows.Forms.Label();
            this.lbdx1 = new System.Windows.Forms.Label();
            this.lbdx2 = new System.Windows.Forms.Label();
            this.tbx2min = new System.Windows.Forms.TextBox();
            this.tbx1max = new System.Windows.Forms.TextBox();
            this.tbx2max = new System.Windows.Forms.TextBox();
            this.tbdx1 = new System.Windows.Forms.TextBox();
            this.tbdx2 = new System.Windows.Forms.TextBox();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbVariant = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // grid
            // 
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Location = new System.Drawing.Point(12, 87);
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.Size = new System.Drawing.Size(541, 226);
            this.grid.TabIndex = 0;
            // 
            // tbx1min
            // 
            this.tbx1min.Location = new System.Drawing.Point(55, 6);
            this.tbx1min.Name = "tbx1min";
            this.tbx1min.Size = new System.Drawing.Size(100, 20);
            this.tbx1min.TabIndex = 1;
            // 
            // lbx1max
            // 
            this.lbx1max.AutoSize = true;
            this.lbx1max.Location = new System.Drawing.Point(165, 9);
            this.lbx1max.Name = "lbx1max";
            this.lbx1max.Size = new System.Drawing.Size(40, 13);
            this.lbx1max.TabIndex = 2;
            this.lbx1max.Text = "X1Max";
            // 
            // lbx2max
            // 
            this.lbx2max.AutoSize = true;
            this.lbx2max.Location = new System.Drawing.Point(165, 64);
            this.lbx2max.Name = "lbx2max";
            this.lbx2max.Size = new System.Drawing.Size(40, 13);
            this.lbx2max.TabIndex = 3;
            this.lbx2max.Text = "X2Max";
            // 
            // lbx1min
            // 
            this.lbx1min.AutoSize = true;
            this.lbx1min.Location = new System.Drawing.Point(12, 9);
            this.lbx1min.Name = "lbx1min";
            this.lbx1min.Size = new System.Drawing.Size(37, 13);
            this.lbx1min.TabIndex = 4;
            this.lbx1min.Text = "X1Min";
            // 
            // lbx2min
            // 
            this.lbx2min.AutoSize = true;
            this.lbx2min.Location = new System.Drawing.Point(12, 64);
            this.lbx2min.Name = "lbx2min";
            this.lbx2min.Size = new System.Drawing.Size(37, 13);
            this.lbx2min.TabIndex = 5;
            this.lbx2min.Text = "X2Min";
            // 
            // lbdx1
            // 
            this.lbdx1.AutoSize = true;
            this.lbdx1.Location = new System.Drawing.Point(328, 9);
            this.lbdx1.Name = "lbdx1";
            this.lbdx1.Size = new System.Drawing.Size(26, 13);
            this.lbdx1.TabIndex = 6;
            this.lbdx1.Text = "dX1";
            // 
            // lbdx2
            // 
            this.lbdx2.AutoSize = true;
            this.lbdx2.Location = new System.Drawing.Point(328, 64);
            this.lbdx2.Name = "lbdx2";
            this.lbdx2.Size = new System.Drawing.Size(26, 13);
            this.lbdx2.TabIndex = 7;
            this.lbdx2.Text = "dX2";
            // 
            // tbx2min
            // 
            this.tbx2min.Location = new System.Drawing.Point(55, 61);
            this.tbx2min.Name = "tbx2min";
            this.tbx2min.Size = new System.Drawing.Size(100, 20);
            this.tbx2min.TabIndex = 8;
            // 
            // tbx1max
            // 
            this.tbx1max.Location = new System.Drawing.Point(211, 6);
            this.tbx1max.Name = "tbx1max";
            this.tbx1max.Size = new System.Drawing.Size(100, 20);
            this.tbx1max.TabIndex = 9;
            // 
            // tbx2max
            // 
            this.tbx2max.Location = new System.Drawing.Point(211, 61);
            this.tbx2max.Name = "tbx2max";
            this.tbx2max.Size = new System.Drawing.Size(100, 20);
            this.tbx2max.TabIndex = 10;
            // 
            // tbdx1
            // 
            this.tbdx1.Location = new System.Drawing.Point(360, 6);
            this.tbdx1.Name = "tbdx1";
            this.tbdx1.Size = new System.Drawing.Size(100, 20);
            this.tbdx1.TabIndex = 11;
            // 
            // tbdx2
            // 
            this.tbdx2.Location = new System.Drawing.Point(360, 61);
            this.tbdx2.Name = "tbdx2";
            this.tbdx2.Size = new System.Drawing.Size(100, 20);
            this.tbdx2.TabIndex = 12;
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(466, 4);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(87, 23);
            this.btnCalculate.TabIndex = 13;
            this.btnCalculate.Text = "Розрахувати";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(466, 59);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(87, 23);
            this.btnClear.TabIndex = 14;
            this.btnClear.Text = "Очистити";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(466, 319);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(87, 23);
            this.btnExit.TabIndex = 15;
            this.btnExit.Text = "Вихід";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 324);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Сума додатних косинусів всіх значень";
            // 
            // tbVariant
            // 
            this.tbVariant.Location = new System.Drawing.Point(218, 321);
            this.tbVariant.Name = "tbVariant";
            this.tbVariant.ReadOnly = true;
            this.tbVariant.Size = new System.Drawing.Size(100, 20);
            this.tbVariant.TabIndex = 17;
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 349);
            this.Controls.Add(this.tbVariant);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.tbdx2);
            this.Controls.Add(this.tbdx1);
            this.Controls.Add(this.tbx2max);
            this.Controls.Add(this.tbx1max);
            this.Controls.Add(this.tbx2min);
            this.Controls.Add(this.lbdx2);
            this.Controls.Add(this.lbdx1);
            this.Controls.Add(this.lbx2min);
            this.Controls.Add(this.lbx1min);
            this.Controls.Add(this.lbx2max);
            this.Controls.Add(this.lbx1max);
            this.Controls.Add(this.tbx1min);
            this.Controls.Add(this.grid);
            this.MaximizeBox = false;
            this.Name = "fMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабораторна робота 9";
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.TextBox tbx1min;
        private System.Windows.Forms.Label lbx1max;
        private System.Windows.Forms.Label lbx2max;
        private System.Windows.Forms.Label lbx1min;
        private System.Windows.Forms.Label lbx2min;
        private System.Windows.Forms.Label lbdx1;
        private System.Windows.Forms.Label lbdx2;
        private System.Windows.Forms.TextBox tbx2min;
        private System.Windows.Forms.TextBox tbx1max;
        private System.Windows.Forms.TextBox tbx2max;
        private System.Windows.Forms.TextBox tbdx1;
        private System.Windows.Forms.TextBox tbdx2;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbVariant;
    }
}

