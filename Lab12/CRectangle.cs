﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab12
{
    class CRectangle : CFigure
    {
        private int _sideA;
        private int _sideB;

        public int SideA {
            get { return _sideA; }
            set {
                _sideA = value >= 200 ? 200 : value;
                _sideA = value <= 5 ? 5 : value;
            }
        }

        public int SideB
        {
            get { return _sideB; }
            set
            {
                _sideB = value >= 200 ? 200 : value;
                _sideB = value <= 5 ? 5 : value;
            }
        }

        public CRectangle (Graphics graphics, int X, int Y, int SideA, int SideB)
        {
            this.graphics = graphics;
            this.X = X;
            this.Y = Y;
            this._sideA = SideA*2;
            this._sideB = SideB*2;
        }

        protected override void Draw(Pen pen)
        {
            Rectangle rectangle = new Rectangle(X-SideA/2, Y-SideB/2, SideA, SideB);
            graphics.DrawRectangle(pen, rectangle);
        }
        public override void Hide()
        {
            Draw(Pens.White);
        }
        public override void Show()
        {
            Draw(Pens.Red);
        }
        public override void Move(int dX, int dY)
        {
            Hide();
            X = X + dX;
            Y = Y + dY;
            Show();
        }

        override public void Expand(int dX) {
            Hide();
            SideA = SideA + dX*2;
            SideB = SideB + dX*2;
            Show();
        }
        override public void Collapse(int dX) {
            Hide();
            SideA = SideA - dX*2;
            SideB = SideB - dX*2;
            Show();
        }
    }
}
