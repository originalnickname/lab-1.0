﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab12
{
    abstract class CFigure
    {
        protected Graphics graphics;
        public int X { get; set; }
        public int Y { get; set; }
        abstract protected void Draw(Pen pen);

        abstract public void Show();

        abstract public void Hide();

        abstract public void Move(int dX, int dY);

        abstract public void Expand(int dR);
        abstract public void Collapse(int dR);
    }
}
