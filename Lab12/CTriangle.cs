﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab12
{
    class CTriangle : CFigure
    {
        private int _side;
        public int Side
        {
            get { return _side; }
            set {
                _side = value >= 200 ? 200 : value;
                _side = value <= 5 ? 5 : value;
            }
        }

        public CTriangle (Graphics graphics, int X, int Y, int Side) {
            this.graphics = graphics;
            this.X = X;
            this.Y = Y;
            this.Side = Side;
        }

        protected override void Draw(Pen pen)
        {
            int r = (int)((Side / 2) / Math.Sin(Math.PI / 3));
            Point p1 = new Point(X + Side, Y);
            Point p2 = new Point(X-r, Y + Side);
            Point p3 = new Point(X-r, Y - Side);
            Point[] triangle = { p1, p2, p3 };
            graphics.DrawPolygon(pen, triangle);
        }
        override public void Expand(int dX)
        {
            Hide();
            Side = Side + dX;
            Show();
        }

        override public void Collapse(int dX) {
            Hide();
            Side = Side - dX;
            Show();
        }
        public override void Hide()
        {
            Draw(Pens.White);
        }
        public override void Show()
        {
            Draw(Pens.Red);
        }
        public override void Move(int dX, int dY)
        {
            Hide();
            X = X + dX;
            Y = Y + dY;
            Show();
        }
    }
}
