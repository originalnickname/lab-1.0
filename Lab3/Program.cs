﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    public class Program
    {
        public delegate double Fnctn (double x);

        public static double FuncInt(double x)
        {
            return Math.Sqrt(Math.Abs((Math.Cos(10 * x) + 0.4) / (160 * x * x)));
        }

        delegate double Integral(double min, double max, double dx, Fnctn Function);

        static void Main(string[] args)
        {
            Integral calc = Trap;
            Fnctn Function = FuncInt;
            Start:
            Console.Write("Введiть початок вiдрiзку iнтегрування a: ");
                string sa = Console.ReadLine();
                double a = double.Parse(sa, System.Globalization.CultureInfo.InvariantCulture);

            Console.Write("Введiть кiнець вiдрiзку iнтегрування b: ");
                string sb = Console.ReadLine();
                double b = double.Parse(sb, System.Globalization.CultureInfo.InvariantCulture);

            Console.Write("Введiть кiлькiсть дiлянок n: ");
                string sn = Console.ReadLine();
                int n = int.Parse(sn, System.Globalization.CultureInfo.InvariantCulture);

            double dx = (b - a) / n;
            double Intgrl = 0;

            Intgrl = calc(a, b, dx, Function);
            Console.WriteLine("Результат обчислень - {0:0.000000}\n", Intgrl);

            Console.Write("Повторити розрахунок (y-так) ? ");
                ConsoleKeyInfo pressedKey = Console.ReadKey();
                Console.WriteLine();
                if (pressedKey.Key == ConsoleKey.Y)
                {
                    Console.Write("1 - Метод трапецiй;\n2 - Метод лiвих прямокутникiв;\n3 - Метод правих прямокутникiв : ");
                    ConsoleKeyInfo method = Console.ReadKey();
                if (method.Key == ConsoleKey.D1 || method.Key == ConsoleKey.NumPad1) calc = Trap;
                else if (method.Key == ConsoleKey.D2 || method.Key == ConsoleKey.NumPad2) calc = Left;
                else if (method.Key == ConsoleKey.D3 || method.Key == ConsoleKey.NumPad3) calc = Right;
                    Console.WriteLine("\n");
                    goto Start;
                }
        }
        public static double Trap(double min, double max, double dx, Fnctn Function)
        {
            Console.WriteLine("\n\n*******Метод трапецiй:*******");
            double intgrl = 0;
            for(double i = min; i < max;)
            {
                double x1 = Function(i);
                i += dx;
                double x2 = Function(i);
                double mid = dx * (x1 + x2) / 2;
                intgrl += mid;
            }
            return intgrl;
        }

        public static double Left(double min, double max, double dx, Fnctn Function)
        {
            Console.WriteLine("*******Метод лiвих прямокутникiв:*******");
            double intgrl = 0;
            for (double i = min; i < max; i += dx)
            {
                intgrl += dx * Function(i);
            }
            return intgrl;
        }

        public static double Right(double min, double max, double dx, Fnctn Function)
        {
            Console.WriteLine("*******Метод правих прямокутникiв:*******");
            double intgrl = 0;
            for (double i = min + dx; i <= max; i += dx)
            {
                intgrl += dx * Function(i);
            }
            return intgrl;
        }
    }
        
}
