﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6Class;

namespace Lab6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the number of teams: ");
            int teamNum = SafeIntParse(Console.ReadLine());
            Team[] teams = InitializeArray<Team>(teamNum);
            int counter = 1;
            foreach(Team team in teams) { 
                string toreplace;
                Console.WriteLine("\n\tTeam {0}\n", counter);
                ++counter;
                Console.Write("Enter the team's name: ");
                toreplace = Console.ReadLine();
                team.Name(toreplace);
                Console.Write("Enter the team's country: ");
                toreplace = Console.ReadLine();
                team.Country(toreplace);
                Console.Write("Enter the team's stadium: ");
                toreplace = Console.ReadLine();
                team.Stadium(toreplace);

                int replace;
                Console.Write("Enter the players number: ");
                replace = SafeIntParse(Console.ReadLine());
                team.Players(replace);
                Console.Write("Enter the coaches number: ");
                replace = SafeIntParse(Console.ReadLine());
                team.Coaches(replace);
                Console.Write("Enter the wins number: ");
                replace = SafeIntParse(Console.ReadLine());
                team.YearWins(replace);
                Console.Write("Enter the losings number: ");
                replace = SafeIntParse(Console.ReadLine());
                team.YearLose(replace);
                Console.Write("Enter the year income: ");
                replace = SafeIntParse(Console.ReadLine());
                team.YearIncome(replace);
            }   //Filling
            foreach (Team a in teams)
            {
                a.Print();
            }     //Printing
            Console.WriteLine("\n");
            while (true)
            {
                ConsoleKeyInfo inpt;
                Console.Write("[{0} to {1}] - Enter the team to operate;", teams.GetLowerBound(0)+1, teams.GetUpperBound(0)+1);
                Console.Write("\nA - print all;\nESC - Exit\nYour choice: ");
                inpt = Console.ReadKey();
                if (inpt.Key == ConsoleKey.Escape) break;
                else if (inpt.Key == ConsoleKey.A)
                {
                    foreach (Team a in teams)
                    {
                        a.Print();
                    }
                    Console.WriteLine("\n");
                }
                else if (SafeIntParse(inpt.KeyChar.ToString()) - 1 >= 0 && SafeIntParse(inpt.KeyChar.ToString()) - 1 <= teams.GetUpperBound(0))
                {
                    counter = SafeIntParse(inpt.KeyChar.ToString()) - 1;
                    while (true)
                    {
                        Console.Write("\n\n");
                        PrintMainMenu();
                        ConsoleKeyInfo mainm = Console.ReadKey();
                        Console.Write("\n\n");
                        if (mainm.Key == ConsoleKey.D1 || mainm.Key == ConsoleKey.NumPad1) teams[counter].Print();
                        else if (mainm.Key == ConsoleKey.D2 || mainm.Key == ConsoleKey.NumPad2) teams[counter].WinRate();
                        else if (mainm.Key == ConsoleKey.D3 || mainm.Key == ConsoleKey.NumPad3) teams[counter].Win();
                        else if (mainm.Key == ConsoleKey.D4 || mainm.Key == ConsoleKey.NumPad4) teams[counter].Lose();
                        else if (mainm.Key == ConsoleKey.D5 || mainm.Key == ConsoleKey.NumPad5)
                        {
                            while (true)
                            {
                                Console.WriteLine("\n\n");
                                Console.WriteLine("\t[1] - Edit Name;");
                                Console.WriteLine("\t[2] - Edit Country;");
                                Console.WriteLine("\t[3] - Edit Stadium;");
                                Console.WriteLine("\t[4] - Edit Players number;");
                                Console.WriteLine("\t[5] - Edit Coaches number;");
                                Console.WriteLine("\t[6] - Edit Year Wins;");
                                Console.WriteLine("\t[7] - Edit Year Loses;");
                                Console.WriteLine("\t[8] - Edit Year Income;");
                                Console.WriteLine("\t[ESC] - Back to Main;");
                                Console.Write("\tYour choice : ");

                                ConsoleKeyInfo editing = Console.ReadKey();

                                if (editing.Key == ConsoleKey.D1 || editing.Key == ConsoleKey.NumPad1)
                                {
                                    Console.Write("\n\n\tCurrent Name value : ");
                                    Console.WriteLine(teams[counter].Name());
                                    Console.Write("\n\tEnter the value to replace: ");
                                    string toreplace = Console.ReadLine();
                                    teams[counter].Name(toreplace);
                                    Console.Write("\n\tCurrent value : ");
                                    Console.WriteLine(teams[counter].Name());
                                }
                                else if (editing.Key == ConsoleKey.D2 || editing.Key == ConsoleKey.NumPad2)
                                {
                                    Console.Write("\n\n\tCurrent Country value : ");
                                    Console.WriteLine(teams[counter].Country());
                                    Console.Write("\n\tEnter the value to replace: ");
                                    string toreplace = Console.ReadLine();
                                    teams[counter].Country(toreplace);
                                    Console.Write("\n\tCurrent value : ");
                                    Console.WriteLine(teams[counter].Country());
                                }
                                else if (editing.Key == ConsoleKey.D3 || editing.Key == ConsoleKey.NumPad3)
                                {
                                    Console.Write("\n\n\tCurrent Stadium value : ");
                                    Console.WriteLine(teams[counter].Stadium());
                                    Console.Write("\n\tEnter the value to replace: ");
                                    string toreplace = Console.ReadLine();
                                    teams[counter].Stadium(toreplace);
                                    Console.Write("\n\tCurrent value : ");
                                    Console.WriteLine(teams[counter].Stadium());
                                }
                                else if (editing.Key == ConsoleKey.D4 || editing.Key == ConsoleKey.NumPad4)
                                {
                                    Console.Write("\n\n\tCurrent Players value : ");
                                    Console.WriteLine(teams[counter].Players());
                                    Console.Write("\n\tEnter the value to replace: ");
                                    int toreplace = SafeIntParse(Console.ReadLine());
                                    teams[counter].Players(toreplace);
                                    Console.Write("\n\tCurrent value : ");
                                    Console.WriteLine(teams[counter].Players());
                                }
                                else if (editing.Key == ConsoleKey.D5 || editing.Key == ConsoleKey.NumPad5)
                                {
                                    Console.Write("\n\n\tCurrent Coaches value : ");
                                    Console.WriteLine(teams[counter].Coaches());
                                    Console.Write("\n\tEnter the value to replace: ");
                                    int toreplace = SafeIntParse(Console.ReadLine());
                                    teams[counter].Coaches(toreplace);
                                    Console.Write("\n\tCurrent value : ");
                                    Console.WriteLine(teams[counter].Coaches());
                                }
                                else if (editing.Key == ConsoleKey.D6 || editing.Key == ConsoleKey.NumPad6)
                                {
                                    Console.Write("\n\n\tCurrent Year Wins value : ");
                                    Console.WriteLine(teams[counter].YearWins());
                                    Console.Write("\n\tEnter the value to replace: ");
                                    int toreplace = SafeIntParse(Console.ReadLine());
                                    teams[counter].YearWins(toreplace);
                                    Console.Write("\n\tCurrent value : ");
                                    Console.WriteLine(teams[counter].YearWins());
                                }
                                else if (editing.Key == ConsoleKey.D7 || editing.Key == ConsoleKey.NumPad7)
                                {
                                    Console.Write("\n\n\tCurrent Year Lose value : ");
                                    Console.WriteLine(teams[counter].YearLose());
                                    Console.Write("\n\tEnter the value to replace: ");
                                    int toreplace = SafeIntParse(Console.ReadLine());
                                    teams[counter].YearLose(toreplace);
                                    Console.Write("\n\tCurrent value : ");
                                    Console.WriteLine(teams[counter].YearLose());
                                }
                                else if (editing.Key == ConsoleKey.D8 || editing.Key == ConsoleKey.NumPad8)
                                {
                                    Console.Write("\n\n\tCurrent Year Income value : ");
                                    Console.WriteLine(teams[counter].YearIncome());
                                    Console.Write("\n\tEnter the value to replace: ");
                                    int toreplace = SafeIntParse(Console.ReadLine());
                                    teams[counter].YearIncome(toreplace);
                                    Console.Write("\n\tCurrent value : ");
                                    Console.WriteLine(teams[counter].YearIncome());
                                }
                                else if (editing.Key == ConsoleKey.Escape) break;
                                else Console.WriteLine("\n\tWrong input!");
                            }
                        }       //EDITING
                        else if (mainm.Key == ConsoleKey.Escape) break;
                        else Console.WriteLine("\n\nWrong input!");
                    }
                    Console.WriteLine("\n");
                }
                else if (inpt.Key != ConsoleKey.A) Console.WriteLine("\n\nWrong input!");
            }
            Console.ReadKey();
            
        }

        static int SafeIntParse(string toParse)
        {
            int res;
            bool success;
            while (true)
            {
                success = int.TryParse(toParse, out res);
                if (success) break;
                Console.Write("\nWrong input! exp integer. Try again: ");
                toParse = Console.ReadLine();
            }
            return res;
        }

        static T[] InitializeArray<T>(int length) where T : new()
        {
            T[] array = new T[length];
            for (int i = 0; i < length; ++i)
            {
                array[i] = new T();
            }

            return array;
        }

        static void PrintMainMenu()
        {
            Console.WriteLine("\n[1] - Team info;");
            Console.WriteLine("[2] - Team stats;");
            Console.WriteLine("[3] - Win;");
            Console.WriteLine("[4] - Lose;");
            Console.WriteLine("[5] - Editing;");
            Console.WriteLine("[ESC] - Back to teams;");
            Console.Write("Your choice : ");
        }
    }
}
