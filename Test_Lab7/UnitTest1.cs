﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab7;

namespace Test_Lab7
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Func_check()
        {
            double x1 = 5, x2 = 10;
            double expexted = -0.029725;
            double actual = Math.Round(fMain.Function(x1, x2), 6);
            Assert.AreEqual(expexted, actual);
        }
        [TestMethod]
        public void Var_check()
        {
            double x1 = -195.8, x2 = -550.5;
            double actual = fMain.Variant(x1, x2);
            Assert.AreEqual(x1, actual);
        }
    }
}
