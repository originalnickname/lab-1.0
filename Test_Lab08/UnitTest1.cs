﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab08;
using Lab08Class;

namespace Test_Lab08
{
    [TestClass]
    public class UnitTest1
    {

        
        [TestMethod]
        public void TestClassEmptyConstruct()
        {
            Lab08Class.Team team = new Team();
            Assert.AreEqual(team.Name(), "N/S");
            Assert.AreEqual(team.Country(), "N/S");
            Assert.AreEqual(team.Stadium(), "N/S");
            Assert.AreEqual(team.Players(), 0);
            Assert.AreEqual(team.Coaches(), 0);
            Assert.AreEqual(team.YearWins(), 0);
            Assert.AreEqual(team.YearLose(), 0);
        }

        [TestMethod]
        public void TestClassConstructor() {
            Lab08Class.Team team = new Team("name", "country", "stadium", 11, 10, 55, 40);
            Assert.AreEqual(team.Name(), "name");
            Assert.AreEqual(team.Country(), "country");
            Assert.AreEqual(team.Stadium(), "stadium");
            Assert.AreEqual(team.Players(), 11);
            Assert.AreEqual(team.Coaches(), 10);
            Assert.AreEqual(team.YearWins(), 55);
            Assert.AreEqual(team.YearLose(), 40);
        }

        [TestMethod]
        public void TestClassSetters() {
            Lab08Class.Team team = new Team();
            team.Name("name");
            Assert.AreEqual(team.Name(), "name");
            team.Country("country");
            Assert.AreEqual(team.Country(), "country");
            team.Stadium("stadium");
            Assert.AreEqual(team.Stadium(), "stadium");
            team.Players(11);
            Assert.AreEqual(team.Players(), 11);
            team.Coaches(10);
            Assert.AreEqual(team.Coaches(), 10);
            team.YearWins(55);
            Assert.AreEqual(team.YearWins(), 55);
            team.YearLose(40);
            Assert.AreEqual(team.YearLose(), 40);
        }
    }
}
