﻿using System;

namespace Lab6Class
{
    public class Team
    {
        protected string name;
            public void Name(string name_)
            {
                this.name = name_;
            }
            public string Name()
            {
                return this.name;
            }

        protected string country;
            public void Country(string country_)
            {
                this.country = country_;
            }
            public string Country()
            {
                return this.country;
            }

        protected string stadium;
            public void Stadium(string stadium_)
            {
                this.stadium = stadium_;
            }
            public string Stadium()
            {
                return this.stadium;
            }

        protected int players;
            public void Players(int players_)
            {
                this.players = players_;
            }
            public int Players()
            {
                return this.players;
            }

        protected int coaches;
            public void Coaches(int coaches_)
            {
                this.coaches = coaches_;
            }
            public int Coaches()
            {
                return this.coaches;
            }

        protected int yearwins;
            public void YearWins(int yearwins_)
            {
                this.yearwins = yearwins_;
            }
            public int YearWins()
            {
                return this.yearwins;
            }

        protected int yearlose;
            public void YearLose(int yearlose_)
            {
                this.yearlose = yearlose_;
            }
            public int YearLose()
            {
                return this.yearlose;
            }

        protected int yearincome;
            public void YearIncome (int yearincome_)
            {
                this.yearincome = yearincome_;
            }
            public int YearIncome()
            {
                return this.yearincome;
            }

        public Team()
        {
            this.name = "N/S";
            this.country = "N/S";
            this.stadium = "N/S";
            this.players = 0;
            this.coaches = 0;
            this.yearwins = 0;
            this.yearlose = 0;
            this.yearincome = 0;
        }

        public void Print()
        {
            Console.WriteLine("\n\n\tTeam \"{0}\"\n\nCountry - {1}, stadium - {2}.",this.name, this.country, this.stadium);
            Console.WriteLine("Has {0} players and {1} coaches.", this.players, this.coaches);
            Console.Write("Won {0} games and lost {1} this year. ", this.yearwins, this.yearlose);
            Console.WriteLine("Year income = {0}.", this.yearincome);
        }

        public void Win() => ++this.yearwins;

        public void Lose() => ++this.yearlose;

        public double WinrD()
        {
            return (double)this.yearwins / this.yearlose;
        }

        public void WinRate()
        {
            Console.WriteLine("Won : {0}; Lost : {1}; Winrate = {2:0.00}. Income = {3}", this.yearwins, this.yearlose, this.WinrD(), this.yearincome);
        }


    }

}
