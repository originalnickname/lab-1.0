﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab5;

namespace Test_Lab5
{
    [TestClass]
    public class UnitTest1
    {
        Lab5.Team team = new Team();   
        [TestMethod]
        public void Name_check()
        {
            Assert.AreEqual("N/S", team.Name());
            team.Name("PASS");
            Assert.AreEqual("PASS", team.Name());
        }
        [TestMethod]
        public void Country_check()
        {
            Assert.AreEqual("N/S", team.Country());
            team.Country("PASS");
            Assert.AreEqual("PASS", team.Country());
        }
        [TestMethod]
        public void Stadium_check()
        {
            Assert.AreEqual("N/S", team.Stadium());
            team.Stadium("PASS");
            Assert.AreEqual("PASS", team.Stadium());
        }
        [TestMethod]
        public void Players_check()
        {
            Assert.AreEqual(0, team.Players());
            team.Players(228);
            Assert.AreEqual(228, team.Players());
        }
        [TestMethod]
        public void Coaches_check()
        {
            Assert.AreEqual(0, team.Coaches());
            team.Coaches(228);
            Assert.AreEqual(228, team.Coaches());
        }
        [TestMethod]
        public void Wins_check()
        {
            Assert.AreEqual(0, team.YearWins());
            team.YearWins(228);
            Assert.AreEqual(228, team.YearWins());
            team.Win();
            Assert.AreEqual(229, team.YearWins());
        }
        [TestMethod]
        public void Lose_check()
        {
            Assert.AreEqual(0, team.YearLose());
            team.YearLose(228);
            Assert.AreEqual(228, team.YearLose());
            team.Lose();
            Assert.AreEqual(229, team.YearLose());
        }
        [TestMethod]
        public void WinRate_check()
        {
            team.YearWins(5);
            team.YearLose(20);
            Assert.AreEqual(0.25, team.WinrD());
        }
    }
}
